/*
authors: Veronika Pontolillo and Amina Turdalieva
date: 17-10-2021
*/ 
public class SubtractonacciSequence extends Sequence{
    private int firstNumber;
    private int secondNumber;

    public SubtractonacciSequence(int firstNumber, int secondNumber){
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public int getTerm(int position) {  
       int i = 2;
       int nextTerm = 0;
       
       if (position == 0){
           nextTerm = this.firstNumber;
           
       }else if (position == 1){
           nextTerm = this.secondNumber;

       } else {
            while(i <= position) {
                nextTerm = this.firstNumber - this.secondNumber;
                this.firstNumber = this.secondNumber;
                this.secondNumber = nextTerm;
                i++;
            }
        }
        return nextTerm;
    }

}


