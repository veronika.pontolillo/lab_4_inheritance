/*
authors: Veronika Pontolillo and Amina Turdalieva
date: 17-10-2021
*/ 
public class FibonacciSequence extends Sequence{
    
    private int firstFibNum;
    private int secondFibNum;

    public FibonacciSequence (int firstFibNum, int secondFibNum){
        this.firstFibNum = firstFibNum;
        this.secondFibNum = secondFibNum;
    }

    public int getTerm(int n) {  
        int i = 2;
        int nextTerm = 0;
        
        if (n == 0){
            nextTerm = this.firstFibNum;

        }else if (n == 1){
            nextTerm = this.secondFibNum;

        } else {
            while(i <= n) {
                nextTerm = this.firstFibNum + this.secondFibNum;
                this.firstFibNum = this.secondFibNum;
                this.secondFibNum = nextTerm;
                i++;
            }
        }
        return nextTerm;
    }
}

