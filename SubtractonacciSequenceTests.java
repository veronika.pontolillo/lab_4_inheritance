/*
authors: Veronika Pontolillo and Amina Turdalieva
date: 17-10-2021
*/ 
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SubtractonacciSequenceTests {

    @Test 
    //checking if get method returns the right value towards the beginning
    public void checkGetTerm(){
        SubtractonacciSequence subFib1 = new SubtractonacciSequence(2, 4);
        assertEquals(-8,subFib1.getTerm(4));
    }

    @Test 
    //checking if get method returns the right value towards the end
    public void checkGetEndTerms(){
        SubtractonacciSequence subFib1 = new SubtractonacciSequence(2,4);
        assertEquals(36, subFib1.getTerm(7));

    }
    @Test 
    //checking if it would fail when at 0 (because there are no previous values)
    public void checkFailsZero(){
        SubtractonacciSequence subFib1 = new SubtractonacciSequence(2,4);
        assertNotEquals(18, subFib1.getTerm(0));
    }

    @Test 
    //checking get method with a different sequence of numbers
    public void checkDiffFib(){
        SubtractonacciSequence subFib1 = new SubtractonacciSequence(0,1);
        assertEquals(-1, subFib1.getTerm(2));
    }
}
