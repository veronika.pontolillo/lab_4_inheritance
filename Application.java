import java.util.Scanner;
public class Application {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Please enter a string of fibonacci and subtractonacci sequences:");
        String userString = reader.next();
        Sequence[] newString = parse(userString);
        print(newString, 10);
    }  

   public static void print(Sequence[] sequence, int n){
        for (int i=0; i<sequence.length; i++){
            Sequence object = sequence[i];
            for (int j=0; j<=n; j++){
                if (object instanceof FibonacciSequence){
                    System.out.println(((FibonacciSequence)object).getTerm(j));      
                } else if (object instanceof SubtractonacciSequence){
                    System.out.println(((SubtractonacciSequence)object).getTerm(j));  
                } else {
                    System.out.println("wrong");
                }
            }
        }
    }

    public static Sequence[] parse(String sequenceType){

        String[] splitString = sequenceType.split(";");

        Sequence[] seq = new Sequence[splitString.length/4];

        for (int a=0; a < seq.length; a++){ 
            for (int i=0; i < splitString.length; i=i+4){
                if (splitString[i].equals("Fib")){
                    int firstFibInt = Integer.parseInt(splitString[i+1]);
                    int secondFibInt = Integer.parseInt(splitString[i+2]);
                    seq[a] = new FibonacciSequence(firstFibInt, secondFibInt);
                }
                else if(splitString[i].equals("Sub")){
                    int firstSubInt = Integer.parseInt(splitString[i+1]);
                    int secondSubInt = Integer.parseInt(splitString[i+2]);
                    seq[a] = new SubtractonacciSequence(firstSubInt, secondSubInt);
                }
            }
        }   
        return seq;
    } 
}
