/*
authors: Veronika Pontolillo and Amina Turdalieva
date: 17-10-2021
*/ 
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestFibonacciSequence {
    
    @Test
    public void testBegginingSequence() {
        //This method should test that the right numbers given to the input of the sequence are returned and are part of this sequence
        FibonacciSequence fibonacci = new FibonacciSequence(0,1);
        assertEquals(0,fibonacci.getTerm(0));
    }
    
    @Test
    public void testSequenceThirdTerm() {
        //This method should test that the sequence produces the right answer for the first value/third term of the sequence.
        FibonacciSequence fibonacci = new FibonacciSequence(0,1);
        assertEquals(1,fibonacci.getTerm(2));
    }

    @Test
    public void testSequenceFifthTerm() {
        //This method should test that the sequence produces the right answer in the middle of the sequence/fifth term.
        FibonacciSequence fibonacci = new FibonacciSequence(2,4);
        assertEquals(16,fibonacci.getTerm(4));
    }

    @Test 
        //This method should test that the sequence fails if the answer is wrong
    public void checkFailsZero(){
        FibonacciSequence fibonacci = new FibonacciSequence(2,4);
        assertNotEquals(18, fibonacci.getTerm(0));
    }
}